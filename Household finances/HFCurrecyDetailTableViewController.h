//
//  HFCurrecyDetailTableViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 01.05.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFCurrency.h"

@interface HFCurrecyDetailTableViewController : HFTableViewController
@property (strong, nonatomic) HFCurrency *currency;
@property (assign, nonatomic) BOOL addMode;

@end
