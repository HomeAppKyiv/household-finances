//
//  HFCurrency.m
//  Household finances
//
//  Created by Kuropatenko Evgeniy on 26.02.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFCurrency1.h"

@implementation HFCurrency1

@synthesize codeISO = _codeISO;


+(void)updateCurrency{
    //to do
}
-(int)codeISO{
    int result = 0;
    if (!_codeISO) {
        return self.codeISO;
    }
    return result;
}
-(double)exchangeRate{
    return [self getExchangeRate];
}

-(double)getExchangeRate{
    double exchangeRate = 1;
    if ( _codeISO == 980) exchangeRate = 1;
    else if (_codeISO == 840) {exchangeRate = 24;}//USD
    else if (_codeISO == 978) {exchangeRate = 28;}//EUR
    else if (_codeISO == 643) {exchangeRate = 0.37;}//EUR
    //to do
    
    return exchangeRate;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self = [self initWithCodeISO];
    }
    return self;
}

- (instancetype)initWithCodeISO:(int)codeISO
{
    self = [super init];
    if (self) {
        _codeISO = codeISO;
        switch (codeISO) {
            case 980:
                _code = @"UAH";
                _name = @"Украинская гривна";
                _symbol = @"₴";
                break;
            case 840   :
                _code = @"USD";
                _name = @"Доллар США";
                _symbol = @"$";
                break;
            case 978:
                _code = @"EUR";
                _name = @"Евро";
                _symbol = @"€";
                break;
            case 643:
                _code = @"RUB";
                _name = @"Российский рубль";
                _symbol = @"₱";
                break;
                
            default:
                _code = @"UAH";
                _name = @"Украинская гривна";
                _symbol = @"₴";
                _codeISO = 980;
                break;
        }
    }
    return self;
}

-(instancetype)initWithCodeISO
{
    return [self initWithCodeISO:980];// UAH as default
}

+(instancetype)currencyWithCodeISO:(int)codeISO{
    id currency = [HFCurrency1 alloc];
    if (currency) {
        currency = [currency initWithCodeISO:codeISO];
    }
    return currency;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", _code];
}
@end
