//
//  HFSetupTableViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 30.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFSetupTableViewController.h"

@interface HFSetupTableViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *privateSwitch;

@end

@implementation HFSetupTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.privateSwitch.on = ((NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey:@"HF_PrivateSetting"]).boolValue;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)privateChanged:(UISwitch *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(sender.on) forKey:@"HF_PrivateSetting"];
}
@end
