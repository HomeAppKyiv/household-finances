//
//  HFWalletDetailsTableViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 01.05.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFWalletDetailsTableViewController.h"
#import "HFCurrency.h"

@interface HFWalletDetailsTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIPickerView *currencyPicker;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSArray *arrayCurrency;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerCurrency;
@property (strong, nonatomic) HFCurrency *tempCurrency;
@property (weak, nonatomic) IBOutlet UITextField *indexField;
-(IBAction)saveWallet:(UIBarButtonItem *)sender;
@end

@implementation HFWalletDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameTextField.text = self.wallet.name;
    self.currencyPicker.delegate = self;
    self.currencyPicker.dataSource = self;
    self.arrayCurrency = [HFCurrency arrayCurrency];
    self.tempCurrency = self.wallet.currency;
    if (!self.wallet.index) {
        self.indexField.text = @"0";
    } else {
        self.indexField.text = [NSString stringWithFormat:@"%ld",(long)self.wallet.index.integerValue];
    }
    if (!self.wallet) {
        [self.pickerCurrency selectRow:0 inComponent:0 animated:YES];
        self.tempCurrency = self.arrayCurrency[0];
        self.nameTextField.text = @"NEW wallet";
    }else{
        [self.pickerCurrency selectRow:[self.arrayCurrency indexOfObject:self.wallet.currency] inComponent:0 animated:YES];
    }
    self.title = self.nameTextField.text;
}

-(void)viewWillDisappear:(BOOL)animated{
    if (self.addMode) {
        self.addMode = !self.addMode;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][0];
    return [sectionInfo numberOfObjects];
    return 10;
}

- (IBAction)saveWallet:(UIBarButtonItem *)sender {
    if (self.nameTextField.text.length ==0) {
        return;
    }
    if (self.addMode) {
        [HFWallets addWalletWithName:self.nameTextField.text currency:self.tempCurrency];
    } else {
        self.wallet.name = self.nameTextField.text;
        self.wallet.currency = self.tempCurrency;
//        self.wallet.index = @(self.indexField.text.intValue);
        [self.wallet updateWallet];}
    self.addMode = NO;
    [self.navigationController popViewControllerAnimated:YES];}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.tempCurrency = self.arrayCurrency[row];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [self.arrayCurrency[row] description];
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    self.fetchedResultsController = [[HFMain sharedInstance] fetchedResultsControllerWithEntityForName:@"Currency" sortKey:@"name" ascending:YES delegate:self];
    
    return _fetchedResultsController;
}
@end
