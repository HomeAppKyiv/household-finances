//
//  HFGoogleMap.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 21.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFPayments.h"
#import <GoogleMaps/GoogleMaps.h>


@interface HFGoogleMap : UIViewController<GMSMapViewDelegate>
@property (weak,nonatomic) HFPayments *payment;
@end
