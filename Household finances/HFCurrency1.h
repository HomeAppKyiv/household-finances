//
//  HFCurrency.h
//  Household finances
//
//  Created by Kuropatenko Evgeniy on 26.02.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HFCurrency1 : NSObject
@property(nonatomic,readonly) int codeISO;
@property(nonatomic,readonly) NSString * name;
@property(nonatomic,readonly) NSString * code;
@property(nonatomic,readonly) NSString * symbol;
@property(nonatomic,readonly) double exchangeRate;

+(void)updateCurrency;//обновим курс валют с интернета
+(instancetype )currencyWithCodeISO:(int)codeISO;

-(double) getExchangeRate;
- (instancetype)initWithCodeISO:(int)codeISO ;
- (instancetype)initWithCodeISO;

@end
