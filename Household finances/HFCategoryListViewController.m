//
//  HFCategoryListViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 30.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFCategoryListViewController.h"
#import <CoreData/CoreData.h>
#import "HFCategory.h"
#import "HFCategoryDetailViewController.h"
#import "PaymentDescriptionTableViewController.h"


@interface HFCategoryListViewController ()
@property (assign, nonatomic) BOOL choiceMode;
@property (weak, nonatomic) IBOutlet UITableView *categoryTable;

- (IBAction)addCategory:(UIBarButtonItem *)sender;
@end

@implementation HFCategoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _managedObjectContext = [HFMain sharedInstance].managedObjectContext;
    self.categoryTable.delegate = self;
    self.categoryTable.dataSource = self;
    if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count-2] isKindOfClass:[PaymentDescriptionTableViewController class]]) {
       self.choiceMode = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"categoryDetail"]) {
        HFCategoryDetailViewController *destinationViewController = [segue destinationViewController];
        if (!sender) {
            destinationViewController.category = nil;
            destinationViewController.addMode = YES;
        } else {
            NSIndexPath *indexPath = [self.categoryTable indexPathForSelectedRow];
            NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
            destinationViewController.category = (HFCategory*)object;
        }
    }
}

- (IBAction)addCategory:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"categoryDetail" sender:nil];
}

#pragma mark - configure cells

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"name"] description];
    if (self.choiceMode) {
        PaymentDescriptionTableViewController *payment = self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
        if (payment.payment.category == object) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if (self.choiceMode) {
        return NO;
    }
    return YES;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.categoryTable beginUpdates];
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    UITableView *tableView = self.categoryTable;
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.categoryTable endUpdates];
}

#pragma mark - delete cells

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [HFCategory deleteCategory:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.choiceMode) {
        NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
        HFCategory *category = (HFCategory *)object;
        if (self.choiceMode) {
            PaymentDescriptionTableViewController *paymentView = self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            paymentView.payment.category = (HFCategory *)category;
            [paymentView.payment updatePayment];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark -
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    self.fetchedResultsController = [[HFMain sharedInstance] fetchedResultsControllerWithEntityForName:@"Category" sortKey:@"name" ascending:YES delegate:self];

    return _fetchedResultsController;
}
@end