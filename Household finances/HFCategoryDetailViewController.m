//
//  HFCategoryDetailViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 30.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFCategoryDetailViewController.h"

@interface HFCategoryDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveCategory;
@property (weak, nonatomic) IBOutlet UISwitch *notCouting;
- (IBAction)saveCategory:(UIBarButtonItem *)sender;

@end

@implementation HFCategoryDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.category) {
        self.categoryTextField.text = self.category.name;
        self.notCouting.on = self.category.notCounting.boolValue;
    }else{
        self.categoryTextField.text = @"NEW category";
    }
    self.title = self.categoryTextField.text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveCategory:(UIBarButtonItem *)sender {
    if (self.categoryTextField.text.length == 0) {
        return;
    }
    if (self.addMode) {
        [HFCategory addCategory:self.categoryTextField.text];
    } else {
        self.category.name = self.categoryTextField.text;
        self.category.notCounting = @(self.notCouting.isOn);
        [self.category updateCategory];}
    self.addMode = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    if (self.addMode) {
        self.addMode = !self.addMode;
    }
}

-(void)setCategory:(HFCategory *)category{
    _category = category;
}

@end
