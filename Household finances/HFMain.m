//
//  HFMain.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 25.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFMain.h"
#import "HFCategory.h"
#import "HFCurrency.h"
#import "HFWallets.h"
#import "HFViewController.h"


NSString * const HFSetCurrentWalletNotification = @"HFSetCurrentWalletNotification";
NSString * const HFWalletsNotification = @"HFWalletsNotification";


@interface HFMain()
@property (strong,nonatomic)    NSArray* wallets;

@end

@implementation HFMain

+(instancetype) sharedInstance {
    static dispatch_once_t pred;
    static id shared = nil;
    dispatch_once(&pred, ^{
        shared = [[super alloc] initUniqueInstance];
    });
    return shared;
}

-(instancetype) initUniqueInstance {
    [[CLLocationManager new]requestAlwaysAuthorization];
    self.locationManager = [[CLLocationManager alloc]init];
//    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager setDelegate:self];
    self.locationManager.distanceFilter = 50;
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    [self.locationManager startUpdatingLocation];
    
    return [super init];
}

-(void)firstRun{
//    [HFCategory deleteCategory];
//    [HFCategory fillCategory];
//    [HFCurrency deleteCurrency];
//    [HFCurrency fillCurrency];
//    [Wallets deleteWallets];
//    [Wallets fillWallets];
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations{
    _currentLocation = locations[0];
}

-(NSDate *)beginningOfDay:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    return [calendar startOfDayForDate:date];
}

-(NSDate *)endOfDay:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [NSDateComponents new];
    components.day = 1;
    
    NSDate *dateTemp = [calendar dateByAddingComponents:components
                                             toDate:date
                                            options:0];
    dateTemp = [dateTemp dateByAddingTimeInterval:-1];
    return dateTemp;
}

-(void)setCurrentWallet:(HFWallets *)currentWallet{
    _currentWallet = currentWallet;
    [[NSNotificationCenter defaultCenter] postNotificationName:HFSetCurrentWalletNotification object:currentWallet];
}

-(NSDate *)beginningOfMonth:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitMonth |NSCalendarUnitYear fromDate:date];
    NSInteger month = components.month;
    NSInteger year = components.year;
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc]init];
    [dateFormater setDateFormat:@"yyyy MM dd"];
    return [dateFormater dateFromString:[NSString stringWithFormat:@"%0ld %0ld 01",(long)year,(long)month]];
}

-(NSDate *)lastDayOfMonth:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents* comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekday fromDate:date];
    [comps setMonth:[comps month]+1];
    [comps setDay:0];
    return [calendar dateFromComponents:comps];
}

-(UIColor *) backGroundColor
{
    //    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backGround"]];
    //    self.view.backgroundColor = [UIColor colorWithRed:51./255. green:153./255. blue:1.0 alpha:1];
    return [UIColor whiteColor];
}
#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
     return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Household_finances" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Household_finances.sqlite"];
    NSError *error = nil;
  
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil
                                                             URL:storeURL options:options error:&error]) {
        NSString *failureReason = @"There was an error creating or loading the application's saved data.";
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    

    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    [HFWallets updateBalanceForAllWallets];
}

- (NSFetchedResultsController *)fetchedResultsControllerWithEntityForName:(NSString *)entityForName
                                                                  sortKey:(NSString*)sortKey
                                                                ascending:(BOOL)ascending
                                                                 delegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    return [self fetchedResultsControllerWithEntityForName:entityForName sortKey:sortKey ascending:ascending predicate:nil sectionNameKeyPath:nil delegate:delegate];
}

- (NSFetchedResultsController *)fetchedResultsControllerWithEntityForName:(NSString *)entityForName
                                                                  sortKey:(NSString*)sortKey
                                                                ascending:(BOOL)ascending
                                                                predicate:(NSPredicate*)predicate
                                                       sectionNameKeyPath:(NSString*)sectionNameKeyPath
                                                                 delegate:(id<NSFetchedResultsControllerDelegate>)delegate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityForName inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    if (sortKey) {
        NSMutableArray *sortKeysArray = [NSMutableArray new];
        NSArray *sertKeys = [sortKey componentsSeparatedByString:@","];
        for (NSString *key in sertKeys) {
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:key ascending:ascending];
            [sortKeysArray addObject:sortDescriptor];
        }
        [fetchRequest setSortDescriptors:sortKeysArray];
    }
    
    if (predicate) {
        [fetchRequest setPredicate:predicate];
    }
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[HFMain sharedInstance].managedObjectContext sectionNameKeyPath:sectionNameKeyPath cacheName:nil];
    
    aFetchedResultsController.delegate = delegate;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return aFetchedResultsController;
}
@end
