//
//  HFViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 11.07.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFViewController.h"

@interface HFViewController ()

@end

@implementation HFViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[HFMain sharedInstance] backGroundColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
