//
//  Wallets.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class HFBalance, HFCurrency, HFPayments,HFCategory;

@interface HFWallets : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) HFCurrency *currency;
@property (nonatomic, retain) NSSet *payments;
@property (nonatomic, retain) HFBalance *balance;
@property (nonatomic, assign) NSNumber *index;
@property (readonly, strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property(nonatomic,readonly) double intBalance;

+ (void)fillWallets;
+ (void)deleteWallets;
+ (void)deleteWallet:(HFWallets *)wallet;
+ (void)updateBalanceForWallet:(HFWallets *)wallet;
+ (void)updateBalanceForAllWallets;
+ (NSArray*)arrayWallets;
+ (NSManagedObject *)addWalletWithName:(NSString*)name currency:(HFCurrency*)currency;

- (void)receipts:(double)value category:(HFCategory*)category;
- (void)flow:(double)value category:(HFCategory*)category;
- (void)updateWallet;
- (NSArray *)maxCategory;
- (NSArray *)maxCategoryWithPeriod:(NSDate *)date;

@end

@interface HFWallets (CoreDataGeneratedAccessors)

- (void)addPaymentsObject:(HFPayments *)value;
- (void)removePaymentsObject:(HFPayments *)value;
- (void)addPayments:(NSSet *)values;
- (void)removePayments:(NSSet *)values;

@end

@interface HFSortContainer :NSObject
@property (strong, nonatomic) HFWallets *category;
@property (assign, nonatomic) double summa;
@end
