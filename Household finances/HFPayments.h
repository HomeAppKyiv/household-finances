//
//  HFPayments.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class HFCategory,HFWallets, NSManagedObject;

@interface HFPayments : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSDate * dateDay;
@property (nonatomic, retain) NSNumber * dateSeconds;
@property (nonatomic, retain) NSString * dateString;
@property (nonatomic, retain) NSNumber * debet;
@property (nonatomic, retain) NSString * idPicture;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSData * picture;
@property (nonatomic, retain) NSNumber * summa;
@property (nonatomic, retain) NSManagedObject *category;
@property (nonatomic, retain) NSManagedObject *wallet;
@property (nonatomic, retain) NSString *desc;

+ (NSManagedObject *) addPaymentWithWallet:(HFWallets*)wallet
                                    summ:(NSNumber *)summ
                                   debet:(BOOL)debet
                                category:(HFCategory *)category;
- (void) updatePayment;
+ (void) deletePayment:(HFPayments *)payment;
- (void) makePicPreview;
@end
