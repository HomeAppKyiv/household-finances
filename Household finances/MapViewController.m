//
//  MapViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 04.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "MapViewController.h"
#import "CoreLocation/CoreLocation.h"
#import "MapKit/MapKit.h"

@interface MapViewController ()<CLLocationManagerDelegate, MKMapViewDelegate>
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) IBOutlet MKMapView *map;

@end

@implementation MapViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.map = [[MKMapView alloc]init];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *loc = locations[0];
    NSLog(@"%@",loc );
}

@end
