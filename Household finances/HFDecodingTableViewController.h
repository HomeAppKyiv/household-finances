//
//  HFDecodingTableViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.06.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFCategory.h"
#import "HFWallets.h"

@interface HFDecodingTableViewController : HFTableViewController <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSArray *categoryArray;
@property (strong, nonatomic) NSDate *currentPeriod;
@property (assign, nonatomic) HFReportMode reportMode;

@end
