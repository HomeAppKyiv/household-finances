//
//  Balance.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFBalance.h"
#import "HFCurrency.h"
//#import "NSManagedObject.h"


@implementation HFBalance

@dynamic period;
@dynamic summa;
@dynamic currency;
@dynamic wallet;

@end
