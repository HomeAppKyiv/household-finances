//
//  HFCategorySortedTableViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 30.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface HFCategorySortedTableViewController : UITableView<NSFetchedResultsControllerDelegate>

@end
