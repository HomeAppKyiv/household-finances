//
//  Currency.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Balance, NSManagedObject;

@interface HFCurrency : NSManagedObject

@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSNumber * codeISO;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * symbol;
@property (nonatomic, retain) NSSet *wallets;
@property (nonatomic, retain) NSSet *balance;

@property (readonly, strong, nonatomic) NSManagedObjectContext* managedObjectContext;

+ (void)fillCurrency;
+ (void)deleteCurrencys;
+ (void) deleteCurrency:(HFCurrency *)currency;
+ (NSArray*)arrayCurrency;
+ (NSManagedObject *)addCurrencyWithName:(NSString*)name code:(NSString *)code codeISO:(NSNumber *)codeISO symbol:(NSString *)symbol;
- (void)updateCurrency;
- (NSArray *)maxCategoryWithPeriod:(NSDate *)date;

@end

@interface HFCurrency (CoreDataGeneratedAccessors)

- (void)addWalletsObject:(NSManagedObject *)value;
- (void)removeWalletsObject:(NSManagedObject *)value;
- (void)addWallets:(NSSet *)values;
- (void)removeWallets:(NSSet *)values;

@end
