//
//  HFCurrencyListViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 30.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HFCurrencyListViewController : HFViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
