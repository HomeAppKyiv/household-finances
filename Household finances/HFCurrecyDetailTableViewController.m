//
//  HFCurrecyDetailTableViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 01.05.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFCurrecyDetailTableViewController.h"

@interface HFCurrecyDetailTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *symbolTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeISOTextField;
- (IBAction)cancelButton:(UIBarButtonItem *)sender;
- (IBAction)saveCurrency:(UIBarButtonItem *)sender;

@end

@implementation HFCurrecyDetailTableViewController

- (IBAction)cancelButton:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.currency) {
        self.nameTextField.text = @"NEW currency";
    } else{
        self.nameTextField.text = self.currency.name;
        self.symbolTextField.text = self.currency.symbol;
        self.codeTextField.text = self.currency.code;
        self.codeISOTextField.text = [NSString stringWithFormat:@"%@",self.currency.codeISO];
    }
    self.title = self.nameTextField.text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    if (self.addMode) {
        self.addMode = !self.addMode;
    }
}
-(void)setCurrency:(HFCurrency *)currency{
    _currency = currency;
}

- (IBAction)saveCurrency:(UIBarButtonItem *)sender {
    if (self.nameTextField.text.length ==0 || self.symbolTextField.text.length ==0 || self.codeTextField.text.length == 0 ||self.codeISOTextField.text.length ==0) {
        return;
    }
    if (self.addMode) {
        [HFCurrency addCurrencyWithName:self.nameTextField.text
                                   code:self.codeTextField.text
                                codeISO:@([self.codeISOTextField.text integerValue])
                                 symbol:self.symbolTextField.text];
    } else {
        self.currency.name = self.nameTextField.text;
        self.currency.symbol = self.symbolTextField.text;
        self.currency.code = self.codeTextField.text;
        self.currency.codeISO = @([self.codeISOTextField.text integerValue]);
        [self.currency updateCurrency];}
    self.addMode = NO;
   [self.navigationController popViewControllerAnimated:YES];
}
@end
