//
//  HFPayments.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFPayments.h"
#import "HFCategory.h"
#import "HFWallets.h"
#import "CoreLocation/CoreLocation.h"
#import <UIKit/UIKit.h>

@implementation HFPayments

@dynamic date;
@dynamic dateDay;
@dynamic dateSeconds;
@dynamic dateString;
@dynamic debet;
@dynamic idPicture;
@dynamic latitude;
@dynamic longitude;
@dynamic picture;
@dynamic summa;
@dynamic category;
@dynamic wallet;
@dynamic desc;

+(NSManagedObject *)addPaymentWithWallet:(HFWallets*)wallet
                                    summ:(NSNumber *)summ
                                   debet:(BOOL)debet
                                category:(HFCategory *)category
{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Payments" inManagedObjectContext:context];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    long timeOffset = [timeZone secondsFromGMTForDate:[NSDate date]];
    timeOffset = 0;
    NSDate *systemDate = [NSDate date];
    NSDate *curDate = [systemDate dateByAddingTimeInterval:timeOffset];
    NSDate *beginOfDay = [[[HFMain sharedInstance] beginningOfDay:systemDate] dateByAddingTimeInterval:timeOffset];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *currentDay = [dateFormatter stringFromDate:beginOfDay];
    
    [newManagedObject setValue:curDate forKey:@"date"];
    [newManagedObject setValue:@(debet) forKey:@"debet"];
    [newManagedObject setValue:beginOfDay forKey:@"dateDay"];
    [newManagedObject setValue:currentDay forKey:@"dateString"];
    [newManagedObject setValue:@([curDate timeIntervalSinceDate:beginOfDay]) forKey:@"dateSeconds"];
    [newManagedObject setValue:debet?summ:@(summ.doubleValue*(-1.0)) forKey:@"summa"];
    [newManagedObject setValue:debet?nil:category forKey:@"category"];
    [newManagedObject setValue:wallet forKey:@"wallet"];
    CLLocation *loc = [HFMain sharedInstance].currentLocation;
    [newManagedObject setValue:[NSNumber numberWithFloat:loc.coordinate.latitude] forKey:@"latitude"];
    [newManagedObject setValue:[NSNumber numberWithFloat:loc.coordinate.longitude] forKey:@"longitude"];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    [HFMain sharedInstance].lastPayment = newManagedObject;
    [[NSNotificationCenter defaultCenter] postNotificationName:HFSetCurrentWalletNotification object:[HFMain sharedInstance].currentWallet];
    [HFWallets updateBalanceForWallet:wallet];
    return newManagedObject;
}

- (void)updatePayment
{
    NSError *error = nil;
    [[HFMain sharedInstance].managedObjectContext save:&error];
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:HFSetCurrentWalletNotification object:[HFMain sharedInstance].currentWallet];
    [HFWallets updateBalanceForWallet:(HFWallets*)self.wallet];
}

+ (void)deletePayment:(HFPayments *)payment
{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    HFWallets *wallet = (HFWallets*)payment.wallet;
    [context deleteObject:payment];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:HFSetCurrentWalletNotification object:[HFMain sharedInstance].currentWallet];
    [HFWallets updateBalanceForWallet:wallet];
}

- (void)makePicPreview
{
    if (self.picture) {
        UIImage *image = [UIImage imageWithData:self.picture];
        CGRect rect = CGRectMake(0,0,40,40);
        UIGraphicsBeginImageContext( rect.size );
        [image drawInRect:rect];
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        NSData *dataPreview = UIImagePNGRepresentation(image);
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = [dirPaths objectAtIndex:0];
        NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:self.idPicture]];
        [dataPreview writeToFile:databasePath atomically:YES];
    }
}
@end
