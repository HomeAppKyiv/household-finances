//
//  ViewController.m
//  Household finances
//
//  Created by Kuropatenko Evgeniy on 26.02.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "ViewController.h"
#import "HFWallets.h"
#import "PaymentDescriptionTableViewController.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) NSArray *walletsArray;
@property (strong, nonatomic) IBOutlet UISegmentedControl *mySegments;

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.table.dataSource = self;
    self.table.delegate = self;
    [self updateWallets];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCurrentWallet:) name:HFSetCurrentWalletNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeWallets:) name:HFWalletsNotification object:nil];
    NSError *error = nil ;
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HFSetCurrentWalletNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HFWalletsNotification object:nil];
}

-(void)changeCurrentWallet:(NSNotification *)notification
{
    NSInteger curInd = [self.walletsArray indexOfObject:notification.object];
    self.mySegments.selectedSegmentIndex = curInd;
    NSError *error = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wallet = %@",[HFMain sharedInstance].currentWallet];
    [self.fetchedResultsController.fetchRequest setPredicate:predicate];
    [NSFetchedResultsController deleteCacheWithName:nil];
    [self.fetchedResultsController performFetch:&error];
    if (error) {
        NSLog(@"error");
    }
    [self.table reloadData];
}

-(void)changeWallets:(NSNotification *)notification
{
    [self updateWallets];
}

-(void)updateWallets
{
    [self.mySegments removeAllSegments];
    self.walletsArray = [HFWallets arrayWallets];
    NSMutableArray *walletNames = [[NSMutableArray alloc]initWithCapacity:[self.walletsArray count]+1];
    for (HFWallets *wallet in self.walletsArray) {
        [walletNames addObject:wallet.name];
        [self.mySegments insertSegmentWithTitle:wallet.name atIndex:[self.walletsArray count]-1 animated:YES];
    }
    [self.mySegments addTarget:self  action:@selector(mySegmentAction:) forControlEvents:UIControlEventValueChanged];
    [self.mySegments sizeToFit];
    NSInteger curInd = [self.walletsArray indexOfObject:[HFMain sharedInstance].currentWallet];
    self.mySegments.selectedSegmentIndex = curInd;
}

-(void)mySegmentAction:(UISegmentedControl *)segment
{
    if (segment.selectedSegmentIndex != self.walletsArray.count) {
        [HFMain sharedInstance].currentWallet = self.walletsArray[(long)segment.selectedSegmentIndex];
    }
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake) {
        if ([HFMain sharedInstance].lastPayment) {
            [HFPayments deletePayment:[HFMain sharedInstance].lastPayment];
            [HFMain sharedInstance].lastPayment = nil;
        }
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    PaymentDescriptionTableViewController *destinationViewController = [segue destinationViewController];
    if ([[segue identifier] isEqualToString:@"paymentDetail"]) {
        if (!sender) {
            destinationViewController.payment = nil;
        } else {
            NSIndexPath *indexPath = [self.table indexPathForSelectedRow];
            NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
            destinationViewController.payment = (HFPayments*)object;}
    }
}

#pragma mark - table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *identifier = @"day";
    UITableViewHeaderFooterView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewHeaderFooterView alloc]initWithReuseIdentifier:identifier];
//        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:0.0 inSection:section]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *currentDay = [dateFormatter stringFromDate:[object valueForKey:@"date"]];
    cell.textLabel.text = currentDay;
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSNumber *debet = [object valueForKey:@"debet"];
    UIColor *currentColor = debet.boolValue?[UIColor blackColor]:[UIColor redColor];
    cell.textLabel.text = [[object valueForKey:@"category"] description];
    cell.textLabel.text = [[object valueForKey:@"category"] valueForKey:@"name"];
//    cell.textLabel.textColor = currentColor;
    NSDate *date = [object valueForKey:@"date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    cell.detailTextLabel.text = [dateFormatter stringFromDate:date];
//    cell.detailTextLabel.textColor = currentColor;
    UIView *cellSubviews = cell.subviews[0];
    for (UILabel *label in cellSubviews.subviews) {
        if (label.tag == 100) {
            NSNumber *summa = [object valueForKey:@"summa"];
            label.text = summa.doubleValue<0? [NSString stringWithFormat:@"%.2f", summa.doubleValue*(-1.0)] :[NSString stringWithFormat:@"%.2f",summa.doubleValue];
            label.textColor = currentColor;
        }
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        HFPayments *payment = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [HFPayments deletePayment:payment];
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wallet == %@",[HFMain sharedInstance].currentWallet];
    self.fetchedResultsController = [[HFMain sharedInstance] fetchedResultsControllerWithEntityForName:@"Payments" sortKey:@"date"  ascending:NO predicate:predicate sectionNameKeyPath:@"dateDay" delegate:self];
    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Payments" inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
//    [fetchRequest setEntity:entity];
//    [fetchRequest setFetchBatchSize:20];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
//    NSArray *sortDescriptors = @[sortDescriptor];
//    
//    [fetchRequest setSortDescriptors:sortDescriptors];
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wallet == %@",[HFMain sharedInstance].currentWallet];
//    [fetchRequest setPredicate:predicate];
//    
//    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[HFMain sharedInstance].managedObjectContext sectionNameKeyPath:@"dateDay" cacheName:nil];
//    aFetchedResultsController.delegate = self;
//    self.fetchedResultsController = aFetchedResultsController;
//    
//    NSError *error = nil;
//    if (![self.fetchedResultsController performFetch:&error]) {
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//        abort();
//    }
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.table beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.table insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.table deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

-(void)controller:(NSFetchedResultsController *)controller
  didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath
    forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.table;
    switch(type){
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.table endUpdates];
}
@end
