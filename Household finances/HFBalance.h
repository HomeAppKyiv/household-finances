//
//  Balance.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Currency, NSManagedObject;

@interface HFBalance : NSManagedObject

@property (nonatomic, retain) NSDate * period;
@property (nonatomic, retain) NSNumber * summa;
@property (nonatomic, retain) Currency *currency;
@property (nonatomic, retain) NSManagedObject *wallet;

@end
