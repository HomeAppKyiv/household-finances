//
//  HFButton.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 11.03.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
//IB_DESIGNABLE
//@interface HFButton : UIButton
@protocol HFButtonDelegate;

@interface HFButton : UIView
@property(weak,nonatomic) IBInspectable NSString* title;
@property(weak,nonatomic) IBInspectable UIColor* color;
//@property(weak,nonatomic)  NSString* title;
@property(weak,nonatomic) id <HFButtonDelegate> delegate;
-(void) pushStart;
-(void) pushEnd;

@end

@protocol HFButtonDelegate
@required
-(void) buttonPresed:(id)button;
@optional

@end