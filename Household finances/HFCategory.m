//
//  HFCategory.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFCategory.h"
#import "HFPayments.h"
@interface HFCategory()
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@end

@implementation HFCategory

@dynamic name;
@dynamic payments;
@dynamic notCounting;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;

-(instancetype)init
{
    self = [super init];
    if (self) {
        _managedObjectContext = [HFMain sharedInstance].managedObjectContext;
    }
    return self;
}

-(NSManagedObjectContext *)managedObjectContext{
    return [HFMain sharedInstance].managedObjectContext;
}

+(void)deleteCategorys
{
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Category"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    request.resultType = NSManagedObjectResultType;
    NSError *error = nil;
    NSArray* result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    for (HFCategory* category in result) {
        [[HFMain sharedInstance].managedObjectContext deleteObject:category];
    }
    [[HFMain sharedInstance].managedObjectContext save:&error];
}

+(void)fillCategory
{
    NSError *error = nil;
    NSArray* categoryArray = @[@"Fuel",@"Food",@"Car",@"Other"];
    for (NSString* cat in categoryArray) {
        HFCategory* category = [NSEntityDescription insertNewObjectForEntityForName:@"Category"
                                                             inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
        category.name = cat;
    }
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    //читаем
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Category"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    request.resultType = NSManagedObjectResultType;
    NSArray* result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (!result) {
        NSLog(@"%@",[error localizedDescription]);
    }else {
        for (HFCategory* object in result) {
            NSLog(@"name = %@",object.name);
        }
    }
    
}

-(void)updateCategory
{
    NSError *error = nil;
   [[HFMain sharedInstance].managedObjectContext save:&error];
}

+(NSManagedObject *)addCategory:(NSString*)name
{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:context];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    [newManagedObject setValue:name forKey:@"name"];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    return newManagedObject;
}

+ (void) deleteCategory:(HFCategory *)category
{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    [context deleteObject:category];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:HFSetCurrentWalletNotification object:[HFMain sharedInstance].currentWallet];
}
@end
