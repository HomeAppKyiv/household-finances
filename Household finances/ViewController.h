//
//  ViewController.h
//  Household finances
//
//  Created by Kuropatenko Evgeniy on 26.02.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : HFViewController<UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


@end

