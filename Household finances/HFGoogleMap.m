//
//  HFGoogleMap.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 21.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFGoogleMap.h"


@interface HFGoogleMap (){
    GMSMapView *mapView_;
    GMSMarker *marker;
}

@end

@implementation HFGoogleMap

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.payment.latitude.doubleValue
                                                            longitude:self.payment.longitude.doubleValue
                                                                 zoom:13];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.delegate = self;
    
    
    self.view = mapView_;
    
    // Creates a marker in the center of the map.
    marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(self.payment.latitude.doubleValue, self.payment.longitude.doubleValue);
//    marker.title = @"Sydney";
//    marker.snippet = @"Australia";
    marker.map = mapView_;
}

-(void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    self.payment.latitude = [NSNumber numberWithFloat:coordinate.latitude];
    self.payment.longitude = [NSNumber numberWithFloat:coordinate.longitude];
    marker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
    [self.payment updatePayment];
//    NSError *error = nil;
//    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
//        NSLog(@"%@",[error localizedDescription]);
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
