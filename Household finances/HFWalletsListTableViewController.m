//
//  HFWalletsListTableViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 01.05.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFWalletsListTableViewController.h"
#import "HFWallets.h"
#import "HFWalletDetailsTableViewController.h"

@interface HFWalletsListTableViewController ()
@property (strong, nonatomic) IBOutlet UITableView *walletsTables;

@end

@implementation HFWalletsListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _managedObjectContext = [HFMain sharedInstance].managedObjectContext;
    self.walletsTables.delegate = self;
    self.walletsTables.dataSource = self;
    self.tableView.allowsSelectionDuringEditing = YES;
    NSMutableArray *buttons = [NSMutableArray arrayWithCapacity:2];
    [buttons addObject:self.navigationItem.rightBarButtonItem];
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editMode:)];
    [buttons addObject:editButton];
    self.navigationItem.rightBarButtonItems = buttons;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) editMode:(UIBarButtonItem*)button
{
    [self.walletsTables setEditing:!self.walletsTables.editing animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    HFWalletDetailsTableViewController *destinationViewController = [segue destinationViewController];
    if ([[segue identifier] isEqualToString:@"walletDetail"]) {
        if (!sender) {
            destinationViewController.wallet = nil;
            destinationViewController.addMode = YES;
        } else {
            NSIndexPath *indexPath = [self.walletsTables indexPathForSelectedRow];
            NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
            destinationViewController.wallet = (HFWallets*)object;}
    }
}

#pragma mark - UITableViewDelegate
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [self.walletsTables beginUpdates];
    HFWallets *wallet1 = [self.fetchedResultsController objectAtIndexPath:sourceIndexPath];
    HFWallets *wallet2 = [self.fetchedResultsController objectAtIndexPath:destinationIndexPath];
    wallet1.index = @(destinationIndexPath.row);
    wallet2.index = @(sourceIndexPath.row);
    [wallet1 updateWallet];
    [wallet2 updateWallet];
    [self.walletsTables endUpdates];
//    [self.walletsTables reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"name"] description];
    cell.detailTextLabel.text = [[object valueForKey:@"currency"] description];
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.walletsTables beginUpdates];
}

-(void)controller:(NSFetchedResultsController *)controller
  didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath
    forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath{
    UITableView *tableView = self.walletsTables;
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.walletsTables endUpdates];
}

- (IBAction)addWallet:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"walletDetail" sender:nil];
}

#pragma mark - delete cells

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [HFWallets deleteWallet:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    self.fetchedResultsController = [[HFMain sharedInstance] fetchedResultsControllerWithEntityForName:@"Wallets" sortKey:@"index" ascending:YES delegate:self];
    
    return _fetchedResultsController;
}
@end
