//
//  HFCurrencyListViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 30.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFCurrencyListViewController.h"
#import "HFCurrecyDetailTableViewController.h"

@interface HFCurrencyListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *currencyTable;

@end

@implementation HFCurrencyListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _managedObjectContext = [HFMain sharedInstance].managedObjectContext;
    self.currencyTable.delegate = self;
    self.currencyTable.dataSource = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"currencyDetail"]) {
        NSIndexPath *indexPath = [self.currencyTable indexPathForSelectedRow];
        NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        HFCurrecyDetailTableViewController *destinationViewController = [segue destinationViewController];
        destinationViewController.currency = (HFCurrency*)object;
        if (!sender) {
            destinationViewController.addMode = YES;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"name"] description];
    cell.detailTextLabel.text = [[object valueForKey:@"symbol"] description];
}

- (IBAction)addCurrency:(UIBarButtonItem *)sender
{
    [self performSegueWithIdentifier:@"currencyDetail" sender:nil];
}


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.currencyTable beginUpdates];
}

-(void)controller:(NSFetchedResultsController *)controller
  didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath
    forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath{
    UITableView *tableView = self.currencyTable;
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.currencyTable endUpdates];
}

#pragma mark - delete cells

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [HFCurrency deleteCurrency:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    self.fetchedResultsController = [[HFMain sharedInstance] fetchedResultsControllerWithEntityForName:@"Currency" sortKey:@"name" ascending:YES delegate:self];
    
    return _fetchedResultsController;
}

@end
