//
//  HFButton.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 11.03.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFButton.h"
#define HF_BUTTON_STATE_OFF 0
#define HF_BUTTON_STATE_PUSH_START 1
#define HF_BUTTON_STATE_PUSH_END 2

@implementation HFButton

NSInteger buttonState;
CGRect tRect;
UIBezierPath *path;

-(instancetype)init
{
    self = [super init];
    if (self) {
        _color = [UIColor blueColor];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    buttonState = HF_BUTTON_STATE_PUSH_START;
    [self setNeedsDisplay];
}

-(void)setTitle:(NSString *)title{
    _title = title;
    [self setNeedsDisplay];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    [self.delegate buttonPresed:self];
    buttonState = HF_BUTTON_STATE_PUSH_END;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    tRect = self.bounds;
    tRect.size.height -=4;
    tRect.size.width -=4;
    tRect.origin.x +=2;
    tRect.origin.y +=2;
    path = [UIBezierPath bezierPathWithOvalInRect:tRect];
    path.lineWidth = 2;
    UIColor* colorLabel = self.color;
    if (buttonState == HF_BUTTON_STATE_OFF) {
        [self.color setStroke];
    }else if (buttonState == HF_BUTTON_STATE_PUSH_START){
        [self.color setStroke];
        [self.color setFill];
        [path fill];
        colorLabel = [UIColor cyanColor];
//        [UIView animateWithDuration:0.75f delay:0.25f options:UIViewAnimationOptionBeginFromCurrentState animations:^{/**/
//        } completion:^(BOOL finished) {/**/}];

    }else {
        [self.color setStroke];
        buttonState = HF_BUTTON_STATE_OFF;
    }
    [path stroke];
    if (!self.title.length) {
        _title = @"?";
    }
    UILabel *labelButton;
    for (UILabel *label in self.subviews) {
        labelButton = label;
    }
    if (labelButton == nil) {
        labelButton = [[UILabel alloc]init];
    }
    labelButton.text = self.title;
    labelButton.textColor = colorLabel;
    labelButton.frame = CGRectMake((self.bounds.size.height-20)/2, (self.bounds.size.width-20)/2, 20, 20);
    labelButton.textAlignment = NSTextAlignmentCenter;
    [self addSubview:labelButton];

}

-(void)setup{
    buttonState = HF_BUTTON_STATE_OFF;
}

-(void)pushStart{
    tRect = self.bounds;
    tRect.size.height -=4;
    tRect.size.width -=4;
    tRect.origin.x +=2;
    tRect.origin.y +=2;
    path = [UIBezierPath bezierPathWithOvalInRect:tRect];
    path.lineWidth = 2;
    [[UIColor blueColor] setStroke];
//    [path stroke];
//    [[UIColor lightGrayColor] setFill];
//    [path fill];
}

-(void)setColor:(UIColor *)color
{
    _color = color;
    [self setNeedsDisplay];
}

-(void)pushEnd{
//    [[UIColor clearColor] setFill];
//    [path fill];
}
@end

