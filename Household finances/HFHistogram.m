//
//  HFHistogram.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 24.03.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFHistogram.h"


@implementation HFHistogram
NSArray *colors;

-(void)setup
{
    self.backgroundColor = [UIColor clearColor];
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)tapOn:(UIGestureRecognizer *)sender
{
    [self.delegate indexOfTap:sender.view.tag];
}

-(void)indexOfTap:(NSInteger)index
{
    [self.delegate indexOfTap:index];
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setupinitWhithMaxValue:(NSInteger)max
                     minValue:(NSInteger)min
                     category:(NSArray *)category
                       values:(NSArray *)values
                     segments:(NSInteger)segments
{
    [self setup];
    self.maxValue = max;
    self.minValue = min;
    self.category = category;
    self.values = values;
    self.segments = segments;
    colors = [NSArray arrayWithObjects:[UIColor greenColor],[UIColor redColor],[UIColor blueColor],[UIColor yellowColor],[UIColor magentaColor],[UIColor orangeColor],[UIColor cyanColor],[UIColor purpleColor],[UIColor whiteColor], nil];
    [self setNeedsDisplay];
}

-(void)drawRect:(CGRect)rect
{
    for (UILabel *label in self.subviews) {
        [label removeFromSuperview];
    }
    CGSize tSize = self.bounds.size;
    NSInteger categoryCout = [self.category count];
    NSInteger categoryWidth = (tSize.width-BORDER*3 - categoryCout*BORDER)/categoryCout;
    //разчерчиваем
    UIBezierPath *path = [[UIBezierPath alloc]init];
    [path moveToPoint:   CGPointMake(BORDER            ,tSize.height-BORDER)];
    [path addLineToPoint:CGPointMake(tSize.width-BORDER,tSize.height-BORDER)];
    [[UIColor blackColor]setStroke];
    path.lineWidth = 2;
    [path stroke];
    UILabel *label0 = [[UILabel alloc]initWithFrame:CGRectMake(0, tSize.height-BORDER*1.5, BORDER, BORDER)];
    label0.font = [UIFont systemFontOfSize:12.];

    label0.text = [NSString stringWithFormat:@"%i",(int)_minValue];
    [self addSubview:label0];
    for (int i=1; i<=MIN(self.segments,categoryCout); i++) {
        NSInteger d = (tSize.height - BORDER*2)/_segments;
        NSInteger y = d*(i-1)+BORDER;
        path = [[UIBezierPath alloc]init];
        path.lineWidth = 1;
        [path moveToPoint:   CGPointMake(BORDER            ,y)];
        [path addLineToPoint:CGPointMake(tSize.width-BORDER,y)];
        [[UIColor grayColor]setStroke];
        [path stroke];
        
        UILabel *label = [[UILabel alloc]init];
        label.font = [UIFont systemFontOfSize:12.];
        label.textAlignment = NSTextAlignmentLeft;
        if (i==1) {
            label.text = [NSString stringWithFormat:@"%i",(int)_maxValue];
        }else{
            label.text = [NSString stringWithFormat:@"%i",(int)((_maxValue/_segments)*(_segments - i+1))];}
        label.frame =CGRectMake(-BORDER/2,  y - BORDER/2, 60, 20);
        [self addSubview:label];
    }
    //добавляем категории
    if (categoryCout) {
        NSDictionary* attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont systemFontOfSize:14.], NSFontAttributeName, nil];
        for (int i=0; i<categoryCout; i++) {
            CGSize textSize = [[NSString stringWithFormat:@"%@",self.category[i]] sizeWithAttributes:attributes];
            NSNumber *tt =self.values[i];
            NSInteger cHeight = tt.intValue * (((tSize.height - BORDER*2)/_maxValue));
            NSInteger startX = BORDER*(2+i) + categoryWidth*i;
            UIView *tView = [[UIView alloc]initWithFrame:CGRectMake(startX, tSize.height - cHeight -BORDER -1, categoryWidth, cHeight)];
            tView.backgroundColor = [UIColor clearColor];
            tView.tag = i;
            [self addSubview:tView];
            
            UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOn:)];
            [tView addGestureRecognizer:tapGR];
            tView.alpha = 0.70;
            __weak id weakSelf = self;
            [UIView animateWithDuration:0.25 delay:0.2*i options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                tView.backgroundColor = colors[i];
            } completion:^(BOOL finished) {
                if (!finished) {
                    return ;
                }
                //добавляем подписи
                UILabel *label = [[UILabel alloc]init];
                label.text = [NSString stringWithFormat:@"%@",self.category[i]];
                label.adjustsFontSizeToFitWidth = YES;
                if (tSize.height > tSize.width){//vertical
                    UIView *tView = [[UIView alloc]initWithFrame:CGRectMake(tSize.width -BORDER/4,textSize.height*i +BORDER*(0.5+i) , textSize.height, textSize.height)];
                    tView.backgroundColor = colors[i];
                    [weakSelf addSubview:tView];
                    label.frame =CGRectMake(tSize.width/2, textSize.height*i +BORDER*(0.5+i), tSize.width/2-textSize.height,textSize.height+5);
                    label.textAlignment = NSTextAlignmentRight;
                }else{//horizontal
                    if (textSize.width >= categoryWidth-BORDER/2) {
                        label.frame =CGRectMake(startX , tSize.height - textSize.height-5, categoryWidth+BORDER,textSize.height+5);
                    }else{
                        label.frame =CGRectMake(startX + (categoryWidth - textSize.width)/2.0, tSize.height - textSize.height-5, categoryWidth+BORDER,textSize.height+5);
                    }
                    label.textAlignment = NSTextAlignmentLeft;
                }
                [weakSelf addSubview:label];

                //надпись над категорией
                UILabel *labelC = [[UILabel alloc]init];
                labelC.font = [UIFont systemFontOfSize:12.];
                labelC.text = [NSString stringWithFormat:@"%7.0f",((NSNumber *)self.values[i]).doubleValue];
                NSDictionary* attributesLabelC = [NSDictionary dictionaryWithObjectsAndKeys:
                                                  [UIFont systemFontOfSize:12.], NSFontAttributeName, nil];
                CGSize textSizeLabelC = [labelC.text sizeWithAttributes:attributesLabelC];
                labelC.frame = CGRectMake(startX -BORDER/2 +(categoryWidth-textSizeLabelC.width)/2, tSize.height - cHeight -BORDER*2, textSizeLabelC.width, textSizeLabelC.height);
                label.adjustsFontSizeToFitWidth = YES;
                [weakSelf addSubview:labelC];            }];

        }
    }
}

#pragma mark - setters
-(void)setMaxValue:(NSInteger)maxValue
{
    _maxValue = maxValue;
    [self setNeedsDisplay];
}

-(void)setMinValue:(NSInteger)minValue
{
    _minValue = minValue;
    [self setNeedsDisplay];
}

-(void)setCategory:(NSArray *)category
{
    _category = category;
    [self setNeedsDisplay];
}

-(void)setValues:(NSArray *)values
{
    _values = values;
    [self setNeedsDisplay];
}

-(void)setSegments:(NSInteger)segments
{
    _segments = segments>10?10:segments;
    [self setNeedsDisplay];
}
@end
