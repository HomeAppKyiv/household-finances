//
//  HFDecodingTableViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.06.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFDecodingTableViewController.h"
#import "HFWallets.h"
#import "HFCategory.h"
#import "PaymentDescriptionTableViewController.h"

@interface HFDecodingTableViewController ()
@property (weak, nonatomic) IBOutlet UITableView *table;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

@end

@implementation HFDecodingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSError *error = nil ;
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        exit(-1);  // Fail
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    if (self.categoryArray.count == 1) {
        [self setTitle:((HFCategory *)self.categoryArray[0]).name];
    } else {
        NSMutableString *titleString = [NSMutableString stringWithString:((HFCategory *)self.categoryArray[0]).name];
        for (NSInteger i = 1; i<self.categoryArray.count; i++) {
            [titleString appendString:@","];
            [titleString appendString:((HFCategory *)self.categoryArray[i]).name];
        }
        [self setTitle:titleString];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


#pragma mark - table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *identifier = @"day";
    UITableViewHeaderFooterView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewHeaderFooterView alloc]initWithReuseIdentifier:identifier];
        cell.contentView.backgroundColor = [UIColor clearColor];
    }
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:0.0 inSection:section]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *currentDay = [dateFormatter stringFromDate:[object valueForKey:@"date"]];
    cell.textLabel.text = currentDay;
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSNumber *debet = [object valueForKey:@"debet"];
    UIColor *currentColor = debet.boolValue?[UIColor blackColor]:[UIColor redColor];
    cell.textLabel.text = [[object valueForKey:@"category"] description];
    cell.textLabel.text = [[object valueForKey:@"category"] valueForKey:@"name"];
    //    cell.textLabel.textColor = currentColor;
    NSDate *date = [object valueForKey:@"date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    cell.detailTextLabel.text = [dateFormatter stringFromDate:date];
    //    cell.detailTextLabel.textColor = currentColor;
    UIView *cellSubviews = cell.subviews[0];
    for (UILabel *label in cellSubviews.subviews) {
        if (label.tag == 100) {
            NSNumber *summa = [object valueForKey:@"summa"];
            label.text = summa.doubleValue<0? [NSString stringWithFormat:@"%.2f", summa.doubleValue*(-1.0)] :[NSString stringWithFormat:@"%.2f",summa.doubleValue];
            label.textColor = currentColor;
        }
    }
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        HFPayments *obj = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [HFPayments deletePayment:obj];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    PaymentDescriptionTableViewController *destinationViewController = [segue destinationViewController];
    if ([[segue identifier] isEqualToString:@"paymentDetail"]) {
        if (!sender) {
            destinationViewController.payment = nil;
        } else {
            NSIndexPath *indexPath = [self.table indexPathForSelectedRow];
            NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
            destinationViewController.payment = (HFPayments*)object;}
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    NSPredicate *predicate = [NSPredicate new];
    if (self.reportMode == HFReportModeWallet) {
        predicate = [NSPredicate predicateWithFormat:@"wallet == %@ AND date >= %@ AND date <=%@ AND category IN %@",
                     [HFMain sharedInstance].currentWallet,
                     self.currentPeriod,
                     [[HFMain sharedInstance] lastDayOfMonth:self.currentPeriod],
                     self.categoryArray];
    } else {
        predicate = [NSPredicate predicateWithFormat:@"wallet.currency == %@ AND date >= %@ AND date <=%@ AND category IN %@",
                     [HFMain sharedInstance].currentWallet.currency,
                     self.currentPeriod,
                     [[HFMain sharedInstance] lastDayOfMonth:self.currentPeriod],
                     self.categoryArray];
    }

    self.fetchedResultsController = [[HFMain sharedInstance] fetchedResultsControllerWithEntityForName:@"Payments" sortKey:@"date"  ascending:NO predicate:predicate sectionNameKeyPath:@"dateDay" delegate:self];
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.table beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.table insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.table deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

-(void)controller:(NSFetchedResultsController *)controller
  didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath
    forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.table;
    switch(type){
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.table endUpdates];
}

@end
