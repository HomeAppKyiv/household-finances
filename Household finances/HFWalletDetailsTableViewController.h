//
//  HFWalletDetailsTableViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 01.05.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFWallets.h"

@interface HFWalletDetailsTableViewController : HFTableViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) HFWallets *wallet;
@property (assign, nonatomic) BOOL addMode;

@end
