//
//  Wallets.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFWallets.h"
#import "HFBalance.h"
#import "HFCurrency.h"
#import "HFPayments.h"
#import "HFContainer.h"

//@interface HFContainer :NSObject
//@property (strong, nonatomic) id object;
//@property (assign, nonatomic) double doubleValue;
//@end


@implementation HFWallets

@dynamic name;
@dynamic currency;
@dynamic payments;
@dynamic balance;
@dynamic index;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize intBalance = _intBalance;

-(double)intBalance
{
    _intBalance = 0;

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"Payments"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wallet =%@",self];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error - %@",error);
    }else {
        for (NSManagedObject *obj in result) {
            _intBalance += ((NSNumber *)[obj valueForKey:@"summa"]).doubleValue;
        }
    }
    return _intBalance;
}

-(void)receipts:(double)value category:(HFCategory*)category
{
    _intBalance +=value;
}

-(void)flow:(double)value category:(HFCategory*)category
{
    _intBalance -=value;
}

//+(void)deleteCurrency
//{
//    NSFetchRequest* request = [[NSFetchRequest alloc]init];
//    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Wallets"
//                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
//    request.entity = decsription;
//    request.resultType = NSManagedObjectResultType;
//    NSError *error = nil;
//    NSArray* result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
//    for (HFCurrency* category in result) {
//        [[HFMain sharedInstance].managedObjectContext deleteObject:category];
//    }
//    [[HFMain sharedInstance].managedObjectContext save:&error];
//}

+ (void) deleteWallet:(HFWallets *)wallet
{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    [context deleteObject:wallet];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:HFSetCurrentWalletNotification object:[HFMain sharedInstance].currentWallet];
    [[NSNotificationCenter defaultCenter] postNotificationName:HFWalletsNotification object:nil];
}

+(void)fillWallets
{
    NSError *error = nil;
    NSArray* currencyArray = [HFCurrency arrayCurrency];
    for (HFCurrency* currency in currencyArray) {

        HFWallets* walet = [NSEntityDescription insertNewObjectForEntityForName:@"Wallets"
                                                             inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
        walet.name = currency.name;
        walet.currency = currency;
        walet.index = @([currencyArray indexOfObject:currency]);
    }
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    //читаем
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Currency"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    decsription = [NSEntityDescription entityForName:@"Wallets"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    request.resultType = NSManagedObjectResultType;
    NSArray* result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (!result) {
        NSLog(@"%@",[error localizedDescription]);
    }else {
        for (HFWallets* object in result) {
            NSLog(@"name = %@",object);
        }
    }
}

+(NSArray*)arrayWallets
{
    NSError *error = nil;
    
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Wallets"
                              inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    request.resultType = NSManagedObjectResultType;
    NSArray *result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (result.count ==0) {
        [self fillWallets];
        result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    }
    return  result;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _managedObjectContext = [HFMain sharedInstance].managedObjectContext;
        _intBalance = 0;
    }
    return self;
}

-(NSManagedObjectContext *)managedObjectContext
{
    return [HFMain sharedInstance].managedObjectContext;
}

+(void)deleteWallets
{
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Wallets"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    request.resultType = NSManagedObjectResultType;
    NSError *error = nil;
    NSArray* result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    for (HFWallets* category in result) {
        [[HFMain sharedInstance].managedObjectContext deleteObject:category];
    }
    [[HFMain sharedInstance].managedObjectContext save:&error];
    [[NSNotificationCenter defaultCenter] postNotificationName:HFWalletsNotification object:nil];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.name,self.currency];
}

-(void)updateWallet
{
    NSError *error = nil;
    [[HFMain sharedInstance].managedObjectContext save:&error];
    [[NSNotificationCenter defaultCenter] postNotificationName:HFWalletsNotification object:self];
    [HFWallets updateBalanceForAllWallets];
}

+ (void)updateBalanceForWallet:(HFWallets *)wallet
{
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.homeAPP.Household-finances"];
//    NSMutableArray *balanceArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"HF_Balance"]];
    NSMutableArray *balanceArray = [NSMutableArray arrayWithArray:[defaults objectForKey:@"HF_Balance"]];
   if (!balanceArray) {
        [HFWallets updateBalanceForAllWallets];
    } else {
        NSDictionary *tempWallet = nil;
        long indexArray = 0;
        for (NSDictionary *walletDict in balanceArray) {
            if ([(NSString *)[walletDict valueForKey:@"wallet"] isEqualToString:wallet.description]) {
                tempWallet = walletDict;
                indexArray = [balanceArray indexOfObject:walletDict];
             }
        }
        if (!tempWallet) {
            [HFWallets updateBalanceForAllWallets];
        } else {
            [balanceArray removeObject:tempWallet];
            [balanceArray insertObject:@{@"wallet":wallet.description,@"balance":@([wallet intBalance])} atIndex:indexArray];
//            [[NSUserDefaults standardUserDefaults] setObject:balanceArray forKey:@"HF_Balance"];
            [defaults setObject:balanceArray forKey:@"HF_Balance"];
        }
    }
    
}

+ (void)updateBalanceForAllWallets
{
    NSArray *wallets = [HFWallets arrayWallets];
    NSMutableArray *balanceArray = [NSMutableArray arrayWithCapacity:wallets.count];
    for (HFWallets *wallet in wallets) {
        double balance = [wallet intBalance];
        [balanceArray addObject:@{@"wallet":wallet.description,@"balance":@(balance)}];
    }
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.homeAPP.Household-finances"];
    [defaults setObject:balanceArray forKey:@"HF_Balance"];
//    [[NSUserDefaults standardUserDefaults] setObject:balanceArray forKey:@"HF_Balance"];
}

+(NSManagedObject *)addWalletWithName:(NSString*)name
                             currency:(HFCurrency*)currency
{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Wallets" inManagedObjectContext:context];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    [newManagedObject setValue:name forKey:@"name"];
    [newManagedObject setValue:currency forKey:@"currency"];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:HFWalletsNotification object:newManagedObject];
    return newManagedObject;
}

-(NSArray *)maxCategory
{
    NSArray * resultArray = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Category"];
    fetchRequest.includesSubentities = NO;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY payments.wallet == %@ AND notCounting == NO",self];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error - %@",error);
    }else {
        if ([result count]) {
            fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Payments"];
            NSMutableArray *paymentsArray = [[NSMutableArray alloc]initWithCapacity:[result count]];
            for (id curCategory in result) {
                
                predicate = [NSPredicate predicateWithFormat:@"wallet == %@ AND summa <0 AND category == %@",[HFMain sharedInstance].currentWallet,curCategory];
                [fetchRequest setPredicate:predicate];
                result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:&error];
                if (((NSNumber *)[result valueForKeyPath:@"@sum.summa"]).doubleValue == 0.0) {
 
                } else{
                    HFContainer *cs = [[HFContainer alloc]init];
                    cs.object = curCategory;
                    cs.doubleValue = ((NSNumber *)[result valueForKeyPath:@"@sum.summa"]).doubleValue * (-1.0);
                    [paymentsArray addObject:cs];
                }
            }
            NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"doubleValue" ascending:NO];
            [paymentsArray sortUsingDescriptors:@[sd]];
            resultArray = [[NSArray alloc]initWithArray:paymentsArray];
        }
    }    
    return resultArray;
}

-(NSArray *)maxCategoryWithPeriod:(NSDate *)date
{
    NSArray * resultArray = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Category"];
    fetchRequest.includesSubentities = NO;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY payments.wallet == %@ AND notCounting == NO",self];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error - %@",error);
    }else {
        if ([result count]) {
            fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Payments"];
            NSMutableArray *paymentsArray = [[NSMutableArray alloc]initWithCapacity:[result count]];
            for (id curCategory in result) {
                
                predicate = [NSPredicate predicateWithFormat:@"wallet == %@ AND summa <0 AND category == %@ AND date >= %@ AND date <=%@",
                             [HFMain sharedInstance].currentWallet,curCategory,date, [[HFMain sharedInstance] lastDayOfMonth:date]];
                [fetchRequest setPredicate:predicate];
                result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:&error];
                if (((NSNumber *)[result valueForKeyPath:@"@sum.summa"]).doubleValue == 0.0) {
                } else{
                    HFContainer *cs = [[HFContainer alloc]init];
                    cs.object = curCategory;
                    cs.doubleValue = ((NSNumber *)[result valueForKeyPath:@"@sum.summa"]).doubleValue * (-1.0);
                    [paymentsArray addObject:cs];
                }
            }
            NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"doubleValue" ascending:NO];
            [paymentsArray sortUsingDescriptors:@[sd]];
            resultArray = [[NSArray alloc]initWithArray:paymentsArray];
        }
    }
    return resultArray;
}
@end
