//
//  HFMain.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 25.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CoreLocation/CoreLocation.h"
#import <UIKit/UIKit.h>
#define HFMAX_CATEGORY_COUNT 5

extern NSString * const HFSetCurrentWalletNotification;
extern NSString * const HFWalletsNotification;

typedef NS_ENUM(NSInteger, HFReportMode) {
    HFReportModeWallet = 0,
    HFReportModeCurrency
};

@class HFWallets,HFPayments;

@interface HFMain : NSObject<CLLocationManagerDelegate,NSFetchedResultsControllerDelegate>


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong,nonatomic) HFWallets *currentWallet;
@property (strong, nonatomic) id lastPayment;
@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *currentLocation;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

+(instancetype) sharedInstance;
-(instancetype) initUniqueInstance;

-(NSDate *)beginningOfDay:(NSDate *)date;
-(NSDate *)endOfDay:(NSDate *)date;
-(NSDate *)beginningOfMonth:(NSDate *)date;
-(NSDate *)lastDayOfMonth:(NSDate *)date;
-(UIColor *) backGroundColor;

- (NSFetchedResultsController *)fetchedResultsControllerWithEntityForName:(NSString *)entityForName
                                                                  sortKey:(NSString*)sortKey
                                                                ascending:(BOOL)ascending
                                                                 delegate:(id<NSFetchedResultsControllerDelegate>)delegate;

- (NSFetchedResultsController *)fetchedResultsControllerWithEntityForName:(NSString *)entityForName
                                                                  sortKey:(NSString*)sortKey
                                                                ascending:(BOOL)ascending
                                                                predicate:(NSPredicate*)predicate
                                                       sectionNameKeyPath:(NSString*)sectionNameKeyPath
                                                                 delegate:(id<NSFetchedResultsControllerDelegate>)delegate;

+(instancetype) alloc __attribute__((unavailable("alloc not available, call sharedInstance instead")));
-(instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+(instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));

#pragma mark -first run
-(void)firstRun;
@end

