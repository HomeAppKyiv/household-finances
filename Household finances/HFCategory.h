//
//  HFCategory.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Payments;

@interface HFCategory : NSManagedObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSNumber *notCounting;
@property (nonatomic, retain) NSSet *payments;

+(void)fillCategory;
+(void)deleteCategorys;
+(HFCategory *)addCategory:(NSString*)name;
-(void)updateCategory;
+ (void) deleteCategory:(HFCategory *)category;

@end

@interface HFCategory (CoreDataGeneratedAccessors)

- (void)addPaymentsObject:(Payments *)value;
- (void)removePaymentsObject:(Payments *)value;
- (void)addPayments:(NSSet *)values;
- (void)removePayments:(NSSet *)values;

@end
