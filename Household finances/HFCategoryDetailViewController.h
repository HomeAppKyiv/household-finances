//
//  HFCategoryDetailViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 30.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFCategory.h"

@interface HFCategoryDetailViewController : HFTableViewController
@property (strong, nonatomic) HFCategory *category;
@property (assign, nonatomic) BOOL addMode;

@end
