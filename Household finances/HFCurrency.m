//
//  Currency.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 27.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFCurrency.h"
#import "HFBalance.h"
#import "HFContainer.h"


@implementation HFCurrency

@dynamic code;
@dynamic codeISO;
@dynamic name;
@dynamic symbol;
@dynamic wallets;
@dynamic balance;

@synthesize managedObjectContext = _managedObjectContext;

-(instancetype)init{
    self = [super init];
    if (self) {
        _managedObjectContext = [HFMain sharedInstance].managedObjectContext;
    }
    return self;
}
-(NSManagedObjectContext *)managedObjectContext{
    return [HFMain sharedInstance].managedObjectContext;
}

+(void)deleteCurrencys{
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Currency"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    request.resultType = NSManagedObjectResultType;
    NSError *error = nil;
    NSArray* result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    for (HFCurrency* category in result) {
        [[HFMain sharedInstance].managedObjectContext deleteObject:category];
    }
    [[HFMain sharedInstance].managedObjectContext save:&error];
}

+ (void) deleteCurrency:(HFCurrency *)currency
{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    [context deleteObject:currency];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:HFSetCurrentWalletNotification object:[HFMain sharedInstance].currentWallet];
}

+(void)fillCurrency{
    NSError *error = nil;

    NSDictionary* currencyUAH = @{@"name":@"UAH",@"code":@"UAH",@"symbol":@"₴",@"codeISO":@"980"};
    NSDictionary* currencyUSD = @{@"name":@"USD",@"code":@"USD",@"symbol":@"$",@"codeISO":@"840"};
    NSDictionary* currencyEUR = @{@"name":@"EUR",@"code":@"EUR",@"symbol":@"€",@"codeISO":@"978"};
    NSDictionary* currencyRUB = @{@"name":@"RUB",@"code":@"RUB",@"symbol":@"₱",@"codeISO":@"643"};
    NSArray* currencyArray = @[currencyUAH,currencyUSD,currencyEUR,currencyRUB];
    for (NSDictionary* curr in currencyArray) {
        HFCurrency* category = [NSEntityDescription insertNewObjectForEntityForName:@"Currency"
                                                             inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
        category.name = [curr objectForKey:@"name" ];
        category.code = [curr objectForKey:@"code" ];
        category.symbol = [curr objectForKey:@"symbol"] ;

        category.codeISO = [[[NSNumberFormatter alloc]init] numberFromString:[curr objectForKey:@"codeISO"]];
    }
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    //читаем
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Currency"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    request.resultType = NSManagedObjectResultType;
    NSArray* result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (!result) {
        NSLog(@"%@",[error localizedDescription]);
    }else {
        for (Currency* object in result) {
            NSLog(@"name = %@",object);
        }
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", self.code];
}

-(void)updateCurrency{
    NSError *error = nil;
    [[HFMain sharedInstance].managedObjectContext save:&error];
}

+(NSArray*)arrayCurrency{
    NSError *error = nil;
    
    NSFetchRequest* request = [[NSFetchRequest alloc]init];
    NSEntityDescription* decsription = [NSEntityDescription entityForName:@"Currency"
                                                   inManagedObjectContext:[HFMain sharedInstance].managedObjectContext];
    request.entity = decsription;
    request.resultType = NSManagedObjectResultType;
    NSArray *result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    if (result.count == 0) {
        [self fillCurrency];
        result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:request error:&error];
    }
    return  result;
}

+(NSManagedObject *)addCurrencyWithName:(NSString*)name
                                   code:(NSString *)code
                                codeISO:(NSNumber *)codeISO
                                 symbol:(NSString *)symbol{
    NSManagedObjectContext *context = [HFMain sharedInstance].managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Currency" inManagedObjectContext:context];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    [newManagedObject setValue:name forKey:@"name"];
    [newManagedObject setValue:code forKey:@"code"];
    [newManagedObject setValue:codeISO forKey:@"codeISO"];
    [newManagedObject setValue:symbol forKey:@"symbol"];
    NSError *error = nil;
    if (![[HFMain sharedInstance].managedObjectContext save:&error]) {
        NSLog(@"%@",[error localizedDescription]);
    }
    return newManagedObject;
}

-(NSArray *)maxCategoryWithPeriod:(NSDate *)date
{
    NSArray * resultArray = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Category"];
    fetchRequest.includesSubentities = NO;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY payments.wallet.currency == %@  AND notCounting == NO",self];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Error - %@",error);
    }else {
        if ([result count]) {
            fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Payments"];
            NSMutableArray *paymentsArray = [[NSMutableArray alloc]initWithCapacity:[result count]];
            for (id curCategory in result) {
                
                predicate = [NSPredicate predicateWithFormat:@"wallet.currency == %@ AND summa <0 AND category == %@ AND date >= %@ AND date <=%@",
                             self,curCategory,date, [[HFMain sharedInstance] lastDayOfMonth:date]];
                [fetchRequest setPredicate:predicate];
                result = [[HFMain sharedInstance].managedObjectContext executeFetchRequest:fetchRequest error:&error];
                if (((NSNumber *)[result valueForKeyPath:@"@sum.summa"]).doubleValue == 0.0) {
                } else{
                    HFContainer *cs = [[HFContainer alloc]init];
                    cs.object = curCategory;
                    cs.doubleValue = ((NSNumber *)[result valueForKeyPath:@"@sum.summa"]).doubleValue * (-1.0);
                    [paymentsArray addObject:cs];
                }
            }
            NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"doubleValue" ascending:NO];
            [paymentsArray sortUsingDescriptors:@[sd]];
            resultArray = [[NSArray alloc]initWithArray:paymentsArray];
        }
    }
    return resultArray;
}
@end
