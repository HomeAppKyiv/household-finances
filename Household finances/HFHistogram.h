//
//  HFHistogram.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 24.03.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#define BORDER 20
@protocol HFHistogramTap <NSObject>
@property (nonatomic, assign)id delegate;
-(void)indexOfTap:(NSInteger)index;
@end

@interface HFHistogram : UIView <HFHistogramTap>
@property (nonatomic, weak) id <HFHistogramTap> delegate;
@property(nonatomic,readwrite) NSInteger minValue;
@property(nonatomic,readwrite) NSInteger maxValue;
@property(nonatomic,readwrite) NSInteger segments;
@property(nonatomic,readwrite) NSArray *category;
@property(nonatomic,readwrite) NSArray *values;

-(void)setupinitWhithMaxValue:(NSInteger)max
                     minValue:(NSInteger)min
                     category:(NSArray *)category
                       values:(NSArray *)values
                     segments:(NSInteger)segments;
-(void)setup;
-(instancetype)init;

@end

