//
//  TodayViewController.m
//  HF Widget
//
//  Created by Eugene Kuropatenko on 11.07.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding>
@property (strong, nonatomic) NSArray *balance;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.homeAPP.Household-finances"];
    self.balance = [defaults valueForKey:@"HF_Balance"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.preferredContentSize = CGSizeMake(self.tableView.contentSize.width, self.balance.count * 25);
}

-(void)viewWillAppear:(BOOL)animated
{
}
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.balance.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    NSDictionary *wallet = [self.balance objectAtIndex:indexPath.row];
    cell.textLabel.text = [wallet objectForKey:@"wallet"];
    double balance = ((NSNumber *)[wallet objectForKey:@"balance"]).doubleValue;
    UIColor *color = balance >= 0?[UIColor lightTextColor]:[UIColor redColor];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%2.2f",balance];
    cell.detailTextLabel.textColor = color;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath");
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSURL *pjURL = [NSURL URLWithString:@"Household-finances://"];
    [self.extensionContext openURL:pjURL completionHandler:nil];
}
@end
