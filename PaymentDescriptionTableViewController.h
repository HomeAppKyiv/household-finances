//
//  PaymentDescriptionTableViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 05.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFPayments.h"

@interface PaymentDescriptionTableViewController : HFTableViewController <UITableViewDelegate>

@property (strong, nonatomic) HFPayments *payment;

@end
