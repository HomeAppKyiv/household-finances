//
//  AddPaymentViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 05.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "HFButton.h"

@interface AddPaymentViewController : HFViewController<HFButtonDelegate,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
