//
//  FotoViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 05.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FotoViewController : HFViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) HFPayments *payment;
@end
