//
//  HFHistogramViewController.h
//  Household finances
//
//  Created by Eugene Kuropatenko on 24.03.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFHistogram.h"
#import "HFButton.h"

@interface HFHistogramViewController : HFViewController <HFHistogramTap, HFButtonDelegate>
@property (strong, nonatomic) NSMutableArray *categoryArray;
@property (strong, nonatomic) NSMutableArray *summArray;
@end
