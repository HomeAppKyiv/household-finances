//
//  PaymentDescriptionTableViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 05.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "PaymentDescriptionTableViewController.h"
#import "HFCategory.h"
#import "HFGoogleMap.h"

@interface PaymentDescriptionTableViewController ()<UITableViewDelegate , UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableViewCell *walletTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *currencyTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *dateTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *categoryTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *summaTextField;
@property (weak, nonatomic) IBOutlet UITableViewCell *mapTextField;
@property (weak, nonatomic) IBOutlet UIImageView *preview;
@property (weak, nonatomic) IBOutlet UITextField *summaField;

- (IBAction)deletePayment:(UIBarButtonItem *)sender;
@end

@implementation PaymentDescriptionTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.walletTextField.detailTextLabel.text = [self.payment.wallet description];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];    
    self.dateTextField.detailTextLabel.text = [dateFormatter stringFromDate:self.payment.date];
    self.categoryTextField.detailTextLabel.text = [self.payment.category valueForKey:@"name"];
    if (self.payment.idPicture) {
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = [dirPaths objectAtIndex:0];
        NSString *databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent:self.payment.idPicture]];
        self.preview.image = [UIImage imageWithContentsOfFile:databasePath];
    } else {
        self.preview.image = nil;}
    self.summaField.delegate = self;
    self.summaField.textColor = self.payment.debet.boolValue?[UIColor blackColor]:[UIColor redColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    self.categoryTextField.detailTextLabel.text = [self.payment.category valueForKey:@"name"];
    self.summaField.text = [NSString stringWithFormat:@"%.2f", self.payment.summa.doubleValue];
}

-(void)viewDidDisappear:(BOOL)animated
{
    CGFloat tSumma = self.summaField.text.doubleValue;
    if (!tSumma) {
        tSumma = self.payment.summa.doubleValue;
    }
    self.payment.debet = tSumma < 0.0?@(NO):@(YES);
    self.payment.summa = @(tSumma);
    [self.payment updatePayment];
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (IBAction)deletePayment:(UIBarButtonItem *)sender {
    [HFPayments deletePayment:self.payment];
    self.payment = nil;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    HFGoogleMap *destinationViewController = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"mapGoogle"]) {
        destinationViewController.payment = self.payment;
    }else if ([segue.identifier isEqualToString:@"foto"]){
        destinationViewController.payment = self.payment;
    }
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""]) {
        self.summaField.text = [NSString stringWithFormat:@"%.2f", self.payment.summa.doubleValue];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
