//
//  AddPaymentViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 05.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "AddPaymentViewController.h"
#import "HFWallets.h"
#import "HFMain.h"
#import "HFCurrency.h"
#import "HFPayments.h"
#import <CoreData/CoreData.h>
#define HFNUM_FAVORITES_CATEGORY 3

@interface AddPaymentViewController ()
@property (weak, nonatomic) IBOutlet HFButton *buttonPlus;
@property (weak, nonatomic) IBOutlet HFButton *buttonMinus;
@property (weak, nonatomic) IBOutlet UITextField *summ;
@property (weak, nonatomic) HFCurrency *currency;
@property (strong, nonatomic) HFWallets *wallet;
@property (weak, nonatomic) IBOutlet UILabel *walletState;
@property (weak, nonatomic) IBOutlet UISegmentedControl *walletsSegmets;
@property (strong, nonatomic) NSArray *walletsArray;
@property (weak, nonatomic) IBOutlet UITableView *categoryTable;
@property (weak, nonatomic) HFCategory *category;
@property (strong, nonatomic) NSMutableArray *favoritesCategory;

@end

@interface HFContainer :NSObject
@property (strong, nonatomic) id object;
@property (assign, nonatomic) double doubleValue;
@end

@implementation AddPaymentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _managedObjectContext = [HFMain sharedInstance].managedObjectContext;
    self.buttonPlus.delegate = self;
    self.buttonPlus.color = self.buttonMinus.color = [UIColor blueColor];
    self.buttonPlus.tag = 1;
    self.buttonPlus.title = @"+";
    self.buttonMinus.tag = 2;
    self.buttonMinus.delegate = self;
    self.buttonMinus.title = @"-";
    self.favoritesCategory = [NSMutableArray arrayWithCapacity:HFNUM_FAVORITES_CATEGORY];
    [self updateWallets];
    [self didChangeCurrentWallet];
    self.categoryTable.delegate = self;
    self.categoryTable.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCurrentWallet:) name:HFSetCurrentWalletNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeWallets:) name:HFWalletsNotification object:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section ==0) {//favorite
        return self.favoritesCategory.count;
    }else{
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][0];
        return [sectionInfo numberOfObjects];}
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell;
    if (section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellHeaderFavorites"];
    }else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellHeaderAll"];}
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *tempValue = nil;
    if ([indexPath indexAtPosition:0] == 0) {
        tempValue = self.favoritesCategory[[indexPath indexAtPosition:1]];
        cell.textLabel.text = [tempValue valueForKey:@"name"];
    }else {
        tempValue = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:[indexPath indexAtPosition:1] inSection:0]];
        cell.textLabel.text = [[tempValue valueForKey:@"name"] description];
    }
    
    if (self.category == (HFCategory *)tempValue) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void)changeCurrentWallet:(NSNotification *)notification
{
    NSInteger curInd = [self.walletsArray indexOfObject:notification.object];
    self.walletsSegmets.selectedSegmentIndex = curInd;
    [self didChangeCurrentWallet];
}

-(void)changeWallets:(NSNotification *)notification
{
    [self updateWallets];
}

-(void)updateWallets
{
    [self.walletsSegmets removeAllSegments];
    self.walletsArray = [HFWallets arrayWallets];
    NSMutableArray *walletNames = [[NSMutableArray alloc]initWithCapacity:[self.walletsArray count]+1];
    for (HFWallets *wallet in self.walletsArray) {
        [walletNames addObject:wallet.name];
        [self.walletsSegmets insertSegmentWithTitle:wallet.name atIndex:[self.walletsArray count]-1 animated:YES];
    }
    [self.walletsSegmets addTarget:self  action:@selector(mySegmentAction:) forControlEvents:UIControlEventValueChanged];
    self.walletsSegmets.selectedSegmentIndex = 0;
    [HFMain sharedInstance].currentWallet = self.walletsArray[self.walletsSegmets.selectedSegmentIndex];
}

-(void)didChangeCurrentWallet
{
    self.wallet = self.walletsArray[self.walletsSegmets.selectedSegmentIndex];
    self.currency = self.wallet.currency;
    NSArray *paymentsArray = [[HFMain sharedInstance].currentWallet maxCategory];
    [self.favoritesCategory removeAllObjects];
    for (HFContainer *cs in paymentsArray) {
        if (self.favoritesCategory.count < HFNUM_FAVORITES_CATEGORY) {
            [self.favoritesCategory addObject:[cs valueForKey:@"object"]];
        }
    }
    if (self.favoritesCategory.count) {
        self.category = self.favoritesCategory[0]; //будет брать первую из Избранных
    } else{
        self.category = nil;
    }
    [self updateStatus];
    [self.categoryTable reloadData];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HFSetCurrentWalletNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HFWalletsNotification object:nil];
}

-(void)mySegmentAction:(UISegmentedControl *)segment
{
    [HFMain sharedInstance].currentWallet = self.walletsArray[(long)segment.selectedSegmentIndex];
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake) {
        if ([HFMain sharedInstance].lastPayment) {
            [HFPayments deletePayment:[HFMain sharedInstance].lastPayment];
            [HFMain sharedInstance].lastPayment = nil;
        }
    }
}

-(void)buttonPresed:(HFButton*)button
{
    NSMutableString *tempString = [[NSMutableString alloc]initWithString:self.summ.text];
    NSRange range;
    range.length = [tempString length];
    range.location = 0;
    [tempString replaceOccurrencesOfString:@"," withString:@"." options:1 range:range];
    HFCategory *tCategory = self.category;
    if (tempString.doubleValue) {
        BOOL debet = NO;
        if (button.tag == 2) {//minus
            [self.wallet flow:tempString.doubleValue category:self.category];
            debet = NO;
        } else {//plus
            [self.wallet receipts:tempString.doubleValue category:self.category];
            debet = YES;}
        [HFPayments addPaymentWithWallet:self.wallet summ:[NSNumber numberWithDouble:tempString.doubleValue] debet:debet category:self.category];
    }
    [self.summ resignFirstResponder];
    [button pushEnd];
    self.summ.text = @"";
    self.category = tCategory;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.summ resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateStatus
{
    self.walletState.text = [NSString stringWithFormat:@"%.2f %@",(double)self.wallet.intBalance,self.currency.symbol];
    if ((double)self.wallet.intBalance >= 0 ) {
        self.walletState.textColor = [UIColor blackColor];
    } else {
        self.walletState.textColor = [UIColor redColor];
    }
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    self.fetchedResultsController = [[HFMain sharedInstance] fetchedResultsControllerWithEntityForName:@"Category" sortKey:@"name"  ascending:YES delegate:self];
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.categoryTable beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.categoryTable;
    NSIndexPath *indexPathModify = [NSIndexPath indexPathForItem:[indexPath indexAtPosition:1] inSection:1];
    NSIndexPath *newIndexPathModify = [NSIndexPath indexPathForItem:[newIndexPath indexAtPosition:1] inSection:1];
    switch(type) {
        case NSFetchedResultsChangeInsert:
            //            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPathModify] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            //            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView deleteRowsAtIndexPaths:@[indexPathModify] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            //            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            [self configureCell:[tableView cellForRowAtIndexPath:indexPathModify] atIndexPath:indexPathModify];
            break;
            
        case NSFetchedResultsChangeMove:
            //            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            //            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView deleteRowsAtIndexPaths:@[indexPathModify] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPathModify] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.categoryTable endUpdates];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath indexAtPosition:0] == 0) {
        self.category = self.favoritesCategory[[indexPath indexAtPosition:1]];
    } else {
        self.category = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:[indexPath indexAtPosition:1] inSection:0]];
    }
    [self.categoryTable reloadData];
}
@end
