//
//  HFHistogramViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 24.03.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "HFHistogramViewController.h"
#import "HFHistogram.h"
#import "HFCategory.h"
#import "HFPayments.h"
#import "HFWallets.h"
#import "HFCurrency.h"
#import "HFDecodingTableViewController.h"
#import "HFContainer.h"


@interface HFHistogramViewController ()
@property (weak, nonatomic) IBOutlet HFHistogram *mainHistogram;
@property (weak, nonatomic) IBOutlet UISegmentedControl *mySegments;
@property (strong, nonatomic) NSArray *walletsArray;
@property (assign, nonatomic) BOOL menuEnabled;
@property (strong, nonatomic) NSDate *currentPeriod;
@property (assign, nonatomic) HFReportMode reportMode;
@property (strong, nonatomic) NSMutableArray *categoryObjectArray;
@end

//@interface HFContainer :NSObject
//@property (strong, nonatomic) id object;
//@property (assign, nonatomic) double doubleValue;
//@end


@implementation HFHistogramViewController 
@synthesize delegate;


- (IBAction)tapSegment:(UISegmentedControl *)sender
{
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.currentPeriod = [[HFMain sharedInstance] beginningOfMonth:[NSDate date]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCurrentWallet:) name:HFSetCurrentWalletNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeWallets:) name:HFWalletsNotification object:nil];
    [self updateWallets];
    [self changeCurrentWallet:nil];
    UIView *fakeBottomView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)-150.0, CGRectGetWidth(self.view.bounds), 150.0)];
    fakeBottomView.tag = 666;
    [self.view addSubview:fakeBottomView];
    self.reportMode = HFReportModeWallet;
   
    self.mainHistogram.delegate = self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HFSetCurrentWalletNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HFWalletsNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    _menuEnabled = NO;
    [self showMenu];
}

-(void)viewWillDisappear:(BOOL)animated
{
    if (self.menuEnabled) {
        [self menuSwitchOnOff];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)indexOfTap:(NSInteger)index
{
    HFDecodingTableViewController *decodingView = [self.storyboard instantiateViewControllerWithIdentifier:@"DecodingView"];
    NSArray *tArray = [NSArray new];
    if (index < HFMAX_CATEGORY_COUNT-1) {
        tArray = @[self.categoryObjectArray[index]];
    }else {
        NSRange range;
        range.location = index;
        range.length = self.categoryObjectArray.count - index;
        NSIndexSet *objIndSet = [[NSIndexSet alloc] initWithIndexesInRange:range];
        tArray = [self.categoryObjectArray objectsAtIndexes:objIndSet];
    }
    decodingView.currentPeriod = self.currentPeriod;
    decodingView.categoryArray = tArray;
    decodingView.reportMode    = self.reportMode;
    
    [self.navigationController pushViewController:decodingView animated:YES];
}

-(void)changeWallets:(NSNotification *)notification
{
    self.reportMode = HFReportModeWallet;
    [self updateWallets];
}

-(void)updateWallets
{
    [self.mySegments removeAllSegments];
    self.walletsArray = [HFWallets arrayWallets];
    NSMutableArray *walletNames = [[NSMutableArray alloc]initWithCapacity:[self.walletsArray count]+1];
    for (HFWallets *wallet in self.walletsArray) {
        [walletNames addObject:wallet.name];
        [self.mySegments insertSegmentWithTitle:wallet.name atIndex:[self.walletsArray count]-1 animated:YES];
    }
    [self.mySegments addTarget:self  action:@selector(mySegmentAction:) forControlEvents:UIControlEventValueChanged];
    [self.mySegments sizeToFit];
    NSInteger curInd = [self.walletsArray indexOfObject:[HFMain sharedInstance].currentWallet];
    self.mySegments.selectedSegmentIndex = curInd;
    [self.categoryObjectArray removeAllObjects];
}

-(void)mySegmentAction:(UISegmentedControl *)segment
{
    if (segment.selectedSegmentIndex != self.walletsArray.count) {
        self.reportMode = HFReportModeWallet;
        [HFMain sharedInstance].currentWallet = self.walletsArray[(long)segment.selectedSegmentIndex];
    }
}

-(void)changeCurrentWallet:(NSNotification *)notification
{
    NSInteger curInd = [self.walletsArray indexOfObject:notification.object];
    self.reportMode = HFReportModeWallet;
    self.mySegments.selectedSegmentIndex = curInd;
    [self updateMaxCategory];
    [self showMenu];
}

-(void) updateMaxCategory
{
    NSArray *paymentsArray = [NSArray new];
    self.categoryObjectArray = [NSMutableArray new];
    if (self.reportMode == HFReportModeWallet) {
        paymentsArray = [[HFMain sharedInstance].currentWallet maxCategoryWithPeriod:self.currentPeriod];
    } else {
        paymentsArray = [[HFMain sharedInstance].currentWallet.currency maxCategoryWithPeriod:self.currentPeriod];
    }
    self.categoryArray = [[NSMutableArray alloc]initWithCapacity:paymentsArray.count];
    self.summArray = [[NSMutableArray alloc]initWithCapacity:paymentsArray.count];
    for (HFContainer *cs in paymentsArray) {
        if (paymentsArray.count > HFMAX_CATEGORY_COUNT) {
            if (self.categoryArray.count < HFMAX_CATEGORY_COUNT) {
                [self.categoryArray addObject:[cs.object valueForKey:@"name"]];
                [self.categoryObjectArray addObject:cs.object];
                [self.summArray addObject:@(cs.doubleValue)];
            }else {
                self.categoryArray[HFMAX_CATEGORY_COUNT-1] = @"Other";
                [self.categoryObjectArray addObject:cs.object];
                self.summArray[HFMAX_CATEGORY_COUNT-1] =  @(((NSNumber *)self.summArray[HFMAX_CATEGORY_COUNT-1]).doubleValue+ cs.doubleValue);
            }
        }
        else{
            [self.categoryArray addObject:[cs.object valueForKey:@"name"]];
            [self.categoryObjectArray addObject:cs.object];
            [self.summArray addObject:@(cs.doubleValue)];
        }
    }
    NSInteger maxValue = 0;
 
    if (self.summArray.count) {
        maxValue = MAX(lround((((NSNumber *)self.summArray[0]).doubleValue)), lround(((NSNumber *)self.summArray[self.summArray.count-1]).doubleValue));
    }
    [self.mainHistogram setupinitWhithMaxValue:maxValue
                                      minValue:0
                                      category:self.categoryArray
                                        values:self.summArray
                                      segments:4];
    self.menuEnabled = NO;
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    NSInteger maxValue = 0;
    if (self.summArray.count) {
        maxValue = MAX(lround((((NSNumber *)self.summArray[0]).doubleValue)), lround(((NSNumber *)self.summArray[self.summArray.count-1]).doubleValue));
    }
    [self.mainHistogram setupinitWhithMaxValue:maxValue
                                      minValue:0
                                      category:self.categoryArray
                                        values:self.summArray
                                      segments:4];
    [self deleteMenu];
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event{
    if (motion == UIEventSubtypeMotionShake) {
        if ([HFMain sharedInstance].lastPayment) {
            [HFPayments deletePayment:[HFMain sharedInstance].lastPayment];
            [HFMain sharedInstance].lastPayment = nil;
        }
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.menuEnabled) {
        UITouch *touch = [touches anyObject];
        if (touch.view.tag == 111 || touch.view.tag == 222) {
            return;
        }
    }
    [self menuSwitchOnOff];
}

-(void) menuSwitchOnOff
{
    _menuEnabled = !_menuEnabled;
    UIView *bottomMenu = [self.view viewWithTag:123];
    UIView *topMenu = [self.view viewWithTag:234];
    if (!bottomMenu) {
        //bottom menu
        bottomMenu = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)-150.0, CGRectGetWidth(self.view.bounds), 150.0)];
        bottomMenu.tag = 123;
        [self.view addSubview:bottomMenu];
        CAGradientLayer *gradientBg = [CAGradientLayer layer];
        gradientBg.frame = bottomMenu.bounds;
        gradientBg.colors = [NSArray arrayWithObjects:
                             (id)[UIColor clearColor].CGColor,
                             (id)[UIColor blackColor].CGColor,
                             nil];
        gradientBg.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
        CALayer *layer = bottomMenu.layer;
        layer.masksToBounds = YES;
        [layer insertSublayer:gradientBg atIndex:0];
        //label
        UILabel *menuLabel = [[UILabel alloc]init];
        menuLabel.tag = 333;
        menuLabel.font = [UIFont systemFontOfSize:20.];
        menuLabel.shadowColor = [UIColor blackColor];
        menuLabel.textColor = [UIColor whiteColor];
        menuLabel.textAlignment = NSTextAlignmentCenter;
        menuLabel.text = [NSString stringWithFormat:@"%@",@"Сентябрь 2015"];
        NSDictionary* attributesLabelC = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont systemFontOfSize:20.], NSFontAttributeName, nil];
        CGSize textSizeLabelC = [menuLabel.text sizeWithAttributes:attributesLabelC];
        menuLabel.frame = CGRectMake((CGRectGetWidth(bottomMenu.bounds) - textSizeLabelC.width)/2.0-25.0,
                                     CGRectGetHeight(bottomMenu.bounds)/2.0 - textSizeLabelC.height,
                                     textSizeLabelC.width+50.0,
                                     textSizeLabelC.height);
        menuLabel.adjustsFontSizeToFitWidth = YES;
        [bottomMenu addSubview:menuLabel];
        [self addMonth:0];


        //buttons
        HFButton *leftButton = [[HFButton alloc] initWithFrame:CGRectMake(10.0, 35.0, 50.0, 50.0)];
        leftButton.backgroundColor = [UIColor clearColor];
        leftButton.color = [UIColor whiteColor];
        leftButton.title = @"<";
        leftButton.delegate = self;
        leftButton.tag = 111;
        [bottomMenu addSubview:leftButton];
        HFButton *rightButton = [[HFButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.bounds)-60, 35.0, 50.0, 50.0)];
        rightButton.backgroundColor = [UIColor clearColor];
        rightButton.color = [UIColor whiteColor];
        rightButton.title = @">";
        rightButton.delegate = self;
        rightButton.tag = 222;
        [bottomMenu addSubview:rightButton];
        //top menu
        topMenu = [[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 150.0)];
        topMenu.tag = 234;
        [self.view addSubview:topMenu];
        gradientBg = [CAGradientLayer layer];
        gradientBg.frame = topMenu.bounds;
        gradientBg.colors = [NSArray arrayWithObjects:
                             (id)[UIColor blackColor].CGColor,
                             (id)[UIColor clearColor].CGColor,
                             nil];
        gradientBg.locations = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:0.0f],
                                [NSNumber numberWithFloat:1.0f],
                                nil];
        layer = topMenu.layer;
        layer.masksToBounds = YES;
        [layer insertSublayer:gradientBg atIndex:0];
        bottomMenu.alpha = topMenu.alpha = 0.0;
        CGRect frame = menuLabel.frame;
        frame.origin.y += 20;
        UISegmentedControl *menuSegmentControl = [[UISegmentedControl alloc]initWithFrame:frame];
        menuSegmentControl.tag = 444;
        menuSegmentControl.tintColor = [UIColor whiteColor];
        [menuSegmentControl insertSegmentWithTitle:@"" atIndex:0 animated:NO];
        [menuSegmentControl addTarget:self action:@selector(tapBottomMenu:) forControlEvents:UIControlEventValueChanged];
        [topMenu addSubview:menuSegmentControl];

    }
    [self showMenu];
}

-(void) buttonPresed:(HFButton *)button
{
    if (button.tag == 111) {
        [self addMonth:-1];
        [self updateMaxCategory];
    } else if (button.tag == 222) {
        [self addMonth:1];
        [self updateMaxCategory];
    }
}

#pragma mark - top menu
-(void) tapBottomMenu:(UISegmentedControl*) bottomMenuControl
{
    self.reportMode = bottomMenuControl.selectedSegmentIndex;
    [self updateMaxCategory];
}

#pragma mark - bottom menu
-(void) showMenu
{
    UIView *bottomMenu = [self.view viewWithTag:123];
    UIView *topMenu = [self.view viewWithTag:234];
    if (self.menuEnabled) {
        bottomMenu.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds)-150.0, CGRectGetWidth(self.view.bounds), 150.0);
        topMenu.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 150.0);
        UISegmentedControl *menuSegmentControl = (UISegmentedControl *)[topMenu viewWithTag:444];
        if (![[menuSegmentControl titleForSegmentAtIndex:0] isEqualToString:[NSString stringWithFormat:@"%@",[HFMain sharedInstance].currentWallet]]) {
            [menuSegmentControl removeAllSegments];
            [menuSegmentControl insertSegmentWithTitle:[NSString stringWithFormat:@"%@",[HFMain sharedInstance].currentWallet]
                                               atIndex:0
                                              animated:YES];
            [menuSegmentControl insertSegmentWithTitle:[NSString stringWithFormat:@"%@",[HFMain sharedInstance].currentWallet.currency]
                                               atIndex:1
                                              animated:YES];
            menuSegmentControl.selectedSegmentIndex = 0;        }

        [UIView animateWithDuration:0.25 animations:^{
            bottomMenu.alpha = topMenu.alpha = 1;
        }];
        
    } else {
        [UIView animateWithDuration:0.25 animations:^{
            bottomMenu.alpha = topMenu.alpha = 0.0;
        } completion:^(BOOL finished) {
            bottomMenu.frame = topMenu.frame = CGRectMake(0, 0, 0, 0);
        }];
    }
}

-(void) deleteMenu
{
    self.menuEnabled = NO;
    [[self.view viewWithTag:123] removeFromSuperview];
    [[self.view viewWithTag:234] removeFromSuperview];
}

-(void) addMonth:(NSInteger)month
{
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComp = [[NSDateComponents alloc]init];
    dateComp.month = month;
    self.currentPeriod = [[HFMain sharedInstance] beginningOfMonth:[calendar dateByAddingComponents:dateComp toDate:self.currentPeriod options:0]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM yyyy"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *currentDay = [dateFormatter stringFromDate:self.currentPeriod];
    ((UILabel*)[[self.view viewWithTag:123] viewWithTag:333]).text = currentDay;
    
}
@end
