//
//  FotoViewController.m
//  Household finances
//
//  Created by Eugene Kuropatenko on 05.04.15.
//  Copyright (c) 2015 homeAPP. All rights reserved.
//

#import "FotoViewController.h"
#import "HFPayments.h"

@interface FotoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *curImage;
@property (assign, nonatomic) CGFloat lastScale;
@property (assign, nonatomic) CGAffineTransform lastTransform;
@property (assign, nonatomic) CGRect originalFrame;

- (IBAction)add:(UIBarButtonItem *)sender;
- (IBAction)removeFoto:(UIBarButtonItem *)sender;
@end

@implementation FotoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.payment.picture) {
        [self.curImage setImage:[UIImage imageWithData:self.payment.picture]];
    }
    self.originalFrame = self.view.frame;
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGesture:)];

    [self.view addGestureRecognizer:pinchGesture];
    self.lastScale = 1.0;
    self.lastTransform = CGAffineTransformIdentity;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    tapGesture.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGesture:)];
    [self.view addGestureRecognizer:panGesture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.curImage = nil;
}

#pragma mark - gestures

-(void) pinchGesture:(UIPinchGestureRecognizer*)pinch
{
    CGFloat scale = 1 - (self.lastScale - pinch.scale);
    scale = MIN(10, scale);
    CGAffineTransform newTransform = CGAffineTransformScale(self.lastTransform,scale, scale);
    if (newTransform.a <= 1) {
        newTransform.a = 1.0;
        newTransform.d = 1.0;
    } else if (newTransform.a >= 10.0 ){
        newTransform.a = 10.0;
        newTransform.d = 10.0;
    }
    [self.curImage setTransform:newTransform];
    if (pinch.state == UIGestureRecognizerStateEnded)
    {
        self.lastScale = 1.0;
        self.lastTransform = self.curImage.transform;
    }
}

-(void)tapGesture:(UITapGestureRecognizer *)tap
{
    CGAffineTransform newTransform = CGAffineTransformMakeScale(1.0, 1.0);
    [UIView animateWithDuration:0.3 animations:^{
        [self.curImage setTransform:newTransform];
        CGRect newFrame = self.curImage.bounds;
        newFrame.origin.x = 0;
        newFrame.origin.y = 0;
        self.curImage.bounds = newFrame;    }];
    self.lastScale = 1.0;
    self.lastTransform = CGAffineTransformIdentity;
    self.view.frame = self.originalFrame;

}

-(void)panGesture:(UIPanGestureRecognizer*)pan
{
    CGPoint translation = [pan translationInView:self.view];
    pan.view.center = CGPointMake(pan.view.center.x + translation.x,
                                         pan.view.center.y + translation.y);
    [pan setTranslation:CGPointMake(0, 0) inView:self.curImage];
    NSLog(@"frame %@",NSStringFromCGRect(self.view.frame));
    NSLog(@"bounds %@",NSStringFromCGRect(self.curImage.frame));
}

#pragma mark - actions
- (IBAction)add:(UIBarButtonItem *)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    } else {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)removeFoto:(UIBarButtonItem *)sender
{
    if (self.payment.picture) {
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = [dirPaths objectAtIndex:0];
        NSString *databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent:self.payment.idPicture]];
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:databasePath error:&error];
        if (error) {
            NSLog(@"%@",error);
        }
        self.payment.picture = nil;
        self.payment.idPicture = nil;
        [self.payment updatePayment];
        self.curImage.image = nil;
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    CFStringRef newUniqueIDString = CFUUIDCreateString(kCFAllocatorDefault, CFUUIDCreate(kCFAllocatorDefault));
    self.payment.picture = UIImageJPEGRepresentation(image, 0.8);
    self.payment.idPicture = [NSString stringWithFormat:@"%@",newUniqueIDString];
    [self.payment updatePayment];
    [self.payment makePicPreview];
    [self.curImage setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
    CFRelease(newUniqueIDString);
}
@end
